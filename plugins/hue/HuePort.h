//
// Created by dankeenan on 5/17/20.
//

#ifndef PLUGINS_HUE_HUEPORT_H_
#define PLUGINS_HUE_HUEPORT_H_

#include <memory>
#include "olad/Port.h"
#include "lib/Bridge.h"
#include "lib/Light.h"
#include "lib/Streamer.h"
#include "HueRdm.h"
#include "ShowFile.h"

namespace ola {
namespace plugin {
namespace hue {

class HueOutputPort : public BasicOutputPort {
 public:
  explicit HueOutputPort(AbstractDevice *parent,
                         std::shared_ptr<lib::Bridge> bridge,
                         std::vector<std::shared_ptr<lib::Light>> lights,
                         std::shared_ptr<ShowFile> showFile,
                         lib::GroupId groupId);

  bool start();
  bool stop();

  bool WriteDMX(const DmxBuffer &buffer, uint8_t priority) override { return WriteDMX(buffer, priority, false); }
  bool WriteDMX(const DmxBuffer &buffer, uint8_t priority, bool internal);
  void SendRDMRequest(ola::rdm::RDMRequest *request_ptr, ola::rdm::RDMCallback *callback) override;
  void RunFullDiscovery(ola::rdm::RDMDiscoveryCallback *on_complete) override;
  void RunIncrementalDiscovery(ola::rdm::RDMDiscoveryCallback *on_complete) override;

  std::string Description() const override { return "Hue Port"; }

 private:
  typedef struct {
    unsigned int count_expected;
    unsigned int count_current;
    bool failed;
    rdm::RDMCallback *callback;
  } BroadcastRequestTracker;
  typedef std::map<ola::rdm::UID, std::unique_ptr<ola::rdm::RDMControllerInterface>> ResponderMap;
  std::shared_ptr<lib::Bridge> m_bridge;
  std::vector<std::shared_ptr<lib::Light>> m_lights;
  // This is presorted for constructing streaming messages.
  std::unordered_map<lib::PersonalityType, std::vector<std::shared_ptr<lib::Light>>> m_pers_lights;
  std::shared_ptr<ShowFile> m_showfile;
  lib::GroupId m_group_id;
  lib::Streamer m_streamer;
  ResponderMap m_responders;

  // ESTA "RESERVED FOR PROTOTYPING/EXPERIMENTAL USE ONLY"
  static const unsigned int MANU_ID = 0x7FF0;
  static const unsigned int START_DEVICE_ID = 0x00000001;

  void runDiscovery(ola::rdm::RDMDiscoveryCallback *callback);
  void handleBroadcastAck(BroadcastRequestTracker *tracker, rdm::RDMReply *reply);
  /**
   * Sort the lights by personality type
   *
   * This is important for performance reasons - a message can only include lights with
   * the same color mode.
   */
  void sortLightsByPersonality();
  /**
   * Blackout the controlled lights.
   */
  void blackout();
  bool m_active = false;
};

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */


#endif //PLUGINS_HUE_HUEPORT_H_
