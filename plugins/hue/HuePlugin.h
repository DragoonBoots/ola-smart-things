//
// Created by dankeenan on 5/16/20.
//

#ifndef PLUGINS_HUE_HUEPLUGIN_H_
#define PLUGINS_HUE_HUEPLUGIN_H_

#include <string>
#include <memory>
#include "olad/Plugin.h"
#include "ola/plugin_id.h"
#include "HueDevice.h"
#include "ShowFile.h"

namespace ola {
class AbstractPlugin;
namespace plugin {
namespace hue {

class HuePlugin : public ola::Plugin {
 public:
  explicit HuePlugin(ola::PluginAdaptor *plugin_adaptor)
      :
      ola::Plugin(plugin_adaptor),
      m_device(nullptr) {}
  std::string Name() const {
    return PLUGIN_NAME;
  }
  std::string Description() const;
  ola_plugin_id Id() const {
    return OLA_PLUGIN_HUE;
  }
  std::string PluginPrefix() const {
    return PLUGIN_PREFIX;
  }

 private:
  static const char PLUGIN_NAME[];
  static const char PLUGIN_PREFIX[];
  static const char DEFAULT_ADDRESS[];
  static const char BRIDGE_ADDRESS_KEY[];
  static const char DEFAULT_DEVICE_NAME[];
  static const char DEVICE_NAME_KEY[];
  static const char USERNAME_KEY[];
  static const char CLIENTKEY_KEY[];

  HueDevice *m_device;
  std::shared_ptr<ShowFile> m_showfile;

  bool SetDefaultPreferences() override;
  bool StartHook() override;
  bool StopHook() override;
};

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif /* PLUGINS_HUE_HUEPLUGIN_H_ */
