//
// Created by dankeenan on 5/17/20.
//

#include "HuePort.h"
#include "lib/Exception.h"
#include "ola/rdm/UIDAllocator.h"

namespace ola {
namespace plugin {
namespace hue {

HueOutputPort::HueOutputPort(AbstractDevice *parent,
                             std::shared_ptr<lib::Bridge> bridge,
                             std::vector<std::shared_ptr<lib::Light>> lights,
                             std::shared_ptr<ShowFile> showFile,
                             lib::GroupId groupId) :
    BasicOutputPort(parent, 1, true, true),
    m_bridge(bridge),
    m_lights(lights),
    m_showfile(showFile),
    m_group_id(groupId),
    m_streamer(m_bridge, m_group_id) {
  // Setup RDM responders
  rdm::UID first_uid(MANU_ID, START_DEVICE_ID);
  rdm::UIDAllocator allocator(first_uid);
  for (const std::shared_ptr<lib::Light> &light : m_lights) {
    std::unique_ptr<rdm::UID> uid(allocator.AllocateNext());
    if (!uid) {
      OLA_WARN << "Out of RDM UIDs to assign to Hue lights.";
      break;
    }
    m_responders.insert({*uid, std::unique_ptr<HueResponder>(new HueResponder(*uid, light, m_bridge, m_showfile))});
  }
  sortLightsByPersonality();
}

bool HueOutputPort::start() {
  try {
    m_streamer.start();
    m_active = true;
  } catch (lib::StreamException &e) {
    OLA_WARN << e.what();
    return false;
  }

  return true;
}

bool HueOutputPort::stop() {
  try {
    m_active = false;
    blackout();
    m_streamer.stop();
  } catch (lib::StreamException &e) {
    OLA_WARN << e.what();
    return false;
  }

  return true;
}

void HueOutputPort::sortLightsByPersonality() {
  for (const std::shared_ptr<lib::Light> &light : m_lights) {
    const lib::PersonalityType &pers_type = light->getPersonalityType();
    if (m_pers_lights.find(pers_type) == m_pers_lights.end()) {
      m_pers_lights.insert({pers_type, std::vector<std::shared_ptr<lib::Light>>()});
    }
    m_pers_lights.at(pers_type).push_back(light);
  }
}

void HueOutputPort::blackout() {
  uint8_t zeros[512]{};
  std::memset(&zeros, 0, sizeof(zeros));
  DmxBuffer buffer(zeros, sizeof(zeros));

  for (unsigned int i = 0; i < 3; i++) {
    WriteDMX(buffer, UINT8_MAX, true);
    sleep(1);
  }
}

bool HueOutputPort::WriteDMX(const DmxBuffer &buffer, uint8_t, bool internal) {
  if (!m_active && !internal) {
    return true;
  }
  for (const auto &item : m_pers_lights) {
    const lib::PersonalityType &pers_type = item.first;
    if (pers_type == lib::PersonalityType::PersNone) {
      // Ignore these lights
      continue;
    }
    const std::vector<std::shared_ptr<lib::Light>> &lights = item.second;
    lib::Streamer::StreamMessage message;
    unsigned short slot_id = 0;
    for (const auto &light : lights) {
      lib::Personality *const pers = light->getPersonality();
      message.colorMode = pers->hueColorMode();
      lib::Streamer::StreamSlot slot;
      slot.lightId = htons(light->getLightId());
      std::vector<uint8_t> dmx(0, pers->footprint());
      for (unsigned int i = 0; i < pers->footprint(); i++) {
        dmx.push_back(buffer.Get(light->getAddress() + i - 1));
      }
      const lib::LightValue &hue_repr = pers->hueRepr(dmx);
      std::memmove(slot.value, hue_repr.data(), sizeof(slot.value));
      message.data[slot_id] = slot;
      slot_id++;
      if (slot_id == lib::Streamer::MAX_SLOTS) {
        // Message is full, send and reset.
        m_streamer.write(message);
        slot_id = 0;
        message = lib::Streamer::StreamMessage();
      }
    }
    m_streamer.write(message);
  }
  return true;
}

void HueOutputPort::SendRDMRequest(ola::rdm::RDMRequest *request_ptr, ola::rdm::RDMCallback *callback) {
  std::unique_ptr<rdm::RDMRequest> request(request_ptr);

  rdm::UID dest = request->DestinationUID();
  if (dest.IsBroadcast()) {
    // Send the request to all responders
    if (m_responders.empty()) {
      rdm::RunRDMCallback(callback, rdm::RDM_WAS_BROADCAST);
    } else {
      auto *tracker = new BroadcastRequestTracker;
      tracker->count_expected = m_responders.size();
      tracker->count_current = 0;
      tracker->failed = false;
      tracker->callback = callback;
      for (const ResponderMap::value_type &item : m_responders) {
        const ResponderMap::mapped_type &responder = item.second;
        responder->SendRDMRequest(request->Duplicate(),
                                  NewSingleCallback(this, &HueOutputPort::handleBroadcastAck, tracker));
      }
    }
  } else {
    // Specific responder
    try {
      const ResponderMap::mapped_type &controller = m_responders.at(dest);
      controller->SendRDMRequest(request.release(), callback);
    } catch (const std::out_of_range &e) {
      rdm::RunRDMCallback(callback, rdm::RDM_UNKNOWN_UID);
    }
  }
}
void HueOutputPort::RunFullDiscovery(ola::rdm::RDMDiscoveryCallback *on_complete) {
  runDiscovery(on_complete);
}
void HueOutputPort::RunIncrementalDiscovery(ola::rdm::RDMDiscoveryCallback *on_complete) {
  runDiscovery(on_complete);
}
void HueOutputPort::runDiscovery(ola::rdm::RDMDiscoveryCallback *callback) {
  rdm::UIDSet uid_set;
  for (const ResponderMap::value_type &item : m_responders) {
    uid_set.AddUID(item.first);
  }
  callback->Run(uid_set);
}

void HueOutputPort::handleBroadcastAck(BroadcastRequestTracker *tracker, rdm::RDMReply *reply) {
  tracker->count_current++;
  if (reply->StatusCode() != rdm::RDM_WAS_BROADCAST) {
    tracker->failed = true;
  }

  if (tracker->count_current == tracker->count_expected) {
    // All devices have responded
    rdm::RunRDMCallback(tracker->callback, tracker->failed ? rdm::RDM_FAILED_TO_SEND : rdm::RDM_WAS_BROADCAST);
    delete tracker;
  }
}

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
