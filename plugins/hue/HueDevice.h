//
// Created by dankeenan on 5/17/20.
//

#ifndef PLUGINS_HUE_HUEDEVICE_H_
#define PLUGINS_HUE_HUEDEVICE_H_

#include <string>
#include "olad/Device.h"
#include "olad/Plugin.h"
#include "lib/Bridge.h"
#include "HuePort.h"
#include "ShowFile.h"

namespace ola {
class AbstractPlugin;
namespace plugin {
namespace hue {

class HueDevice : public Device {
 public:
  explicit HueDevice(AbstractPlugin *owner, std::shared_ptr<lib::Bridge> bridge, std::shared_ptr<ShowFile> showFile) :
      Device(owner, DEVICE_NAME), m_bridge(std::move(bridge)), m_showfile(showFile), m_group_id(0) {}

  bool AllowLooping() const override {
    return false;
  }

  bool AllowMultiPortPatching() const override {
    return true;
  }

  std::string DeviceId() const override {
    return "1";
  }

 protected:
  bool StartHook() override;
  void PrePortStop() override;

 private:
  static const char DEVICE_NAME[];
  std::shared_ptr<lib::Bridge> m_bridge;
  std::shared_ptr<ShowFile> m_showfile;
  lib::GroupId m_group_id;
  HueOutputPort* m_port = nullptr;
};

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif //PLUGINS_HUE_HUEDEVICE_H_
