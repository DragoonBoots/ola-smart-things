# LIBRARIES
##################################################
if USE_HUE
# This is a library which isn't coupled to olad
noinst_LTLIBRARIES += plugins/hue/libhuelib.la
plugins_hue_libhuelib_la_SOURCES = \
	plugins/hue/lib/Bridge.h \
	plugins/hue/lib/Bridge.cpp \
	plugins/hue/lib/Exception.h \
	plugins/hue/lib/Light.h \
	plugins/hue/lib/Light.cpp \
	plugins/hue/lib/LightModel.h \
	plugins/hue/lib/Streamer.h \
	plugins/hue/lib/Streamer.cpp
plugins_hue_libhuelib_la_CXXFLAGS = $(COMMON_CXXFLAGS) $(libcpprest_CFLAGS) $(gnutls_CFLAGS)
plugins_hue_libhuelib_la_LIBADD = $(libcpprest_LIBS) $(gnutls_LIBS)

lib_LTLIBRARIES += plugins/hue/libolahue.la

# Plugin description is generated from README.md
built_sources += plugins/hue/HuePluginDescription.h
nodist_plugins_hue_libolahue_la_SOURCES = \
    plugins/hue/HuePluginDescription.h
plugins/hue/HuePluginDescription.h: plugins/hue/README.md plugins/hue/Makefile.mk plugins/convert_README_to_header.sh
	sh $(top_srcdir)/plugins/convert_README_to_header.sh $(top_srcdir)/plugins/hue $(top_builddir)/plugins/hue/HuePluginDescription.h

plugins_hue_libolahue_la_SOURCES = \
	plugins/hue/HuePlugin.h \
	plugins/hue/HuePlugin.cpp \
	plugins/hue/HueDevice.h \
	plugins/hue/HueDevice.cpp \
	plugins/hue/HuePort.h \
	plugins/hue/HuePort.cpp \
	plugins/hue/ShowFile.h \
	plugins/hue/ShowFile.cpp \
	plugins/hue/HueRdm.h \
	plugins/hue/HueRdm.cpp
plugins_hue_libolahue_la_CXXFLAGS = $(COMMON_CXXFLAGS)
plugins_hue_libolahue_la_LIBADD = \
    olad/plugin_api/libolaserverplugininterface.la \
    plugins/hue/libhuelib.la

endif

EXTRA_DIST += plugins/hue/README.md
