//
// Created by dankeenan on 5/17/20.
//

#ifndef PLUGINS_HUE_LIB_LIGHT_H_
#define PLUGINS_HUE_LIB_LIGHT_H_

#include <string>
#include <array>
#include <endian.h>
#include <limits>
#include <memory>
#include <vector>
#include <cpprest/json.h>

#define BYTE_HI(val) ((val & 0xFF00u) >> 8u);
#define BYTE_LO(val) (val & 0x00FFu);
#define JOIN_BYTES(hi, lo) ((hi << 8u) + lo)

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

typedef std::array<unsigned char, 6> LightValue;
typedef unsigned char LightId;
typedef unsigned char GroupId;

enum HueColorMode : uint8_t {
  HueColorModeRgb = 0,
  HueColorModeXyz = 1,
};

enum PersonalityType : uint8_t {
  PersNone = 0,
  PersRgbDirect = 1,
  PersRgbCalibrated = 2,
  PersCieXyz = 3,
};

/**
 * Base class for personality handlers
 */
class Personality {
 public:
  static std::unordered_map<std::string, PersonalityType> nameToTypeMap();
  static PersonalityType nameToType(const std::string &name);
  static std::unordered_map<PersonalityType, std::string> typeToNameMap();
  static std::string typeToName(const PersonalityType &personality);

  /**
   * Personality type id
   * @return
   */
  virtual PersonalityType type() const = 0;

  /**
   * Gets the footprint of this personality.
   * @return
   */
  virtual unsigned short footprint() const = 0;

  /**
   * The hue color mode value
   * @see https://developers.meethue.com/develop/hue-entertainment/philips-hue-entertainment-api/#getting-started-with-streaming-api
   * @return
   */
  virtual HueColorMode hueColorMode() const = 0;

  /**
   * The byte representation of this value
   * @see https://developers.meethue.com/develop/hue-entertainment/philips-hue-entertainment-api/#getting-started-with-streaming-api
   * @return
   */
  virtual LightValue hueRepr(const std::vector<uint8_t> &dmx) const = 0;
};

/**
 * Dummy personality to represent an unused light.
 */
class NonePersonality : public Personality {
 public:
  PersonalityType type() const override {
    return PersonalityType::PersNone;
  }
  unsigned short footprint() const override {
    return 0;
  }
  HueColorMode hueColorMode() const override {
    return HueColorMode::HueColorModeRgb;
  }
  LightValue hueRepr(const std::vector<uint8_t> &) const override {
    return {0, 0, 0, 0, 0, 0};
  }
};

/**
 * RGB-type personality
 *
 * Uses 16-bit values for Red, Green, and Blue.
 */
class RgbDirectPersonality : public Personality {
 public:
  explicit RgbDirectPersonality() = default;

  PersonalityType type() const override {
    return PersonalityType::PersRgbDirect;
  }
  unsigned short footprint() const override {
    return 6;
  }
  HueColorMode hueColorMode() const override {
    return HueColorMode::HueColorModeRgb;
  }

  LightValue hueRepr(const std::vector<uint8_t> &dmx) const override;
};

/**
 * CIE XY Personality
 *
 * Uses 16-bit values to represent X, Y, and Intensity
 */
class CieXyzPersonality : public Personality {
 public:
  explicit CieXyzPersonality() = default;

  PersonalityType type() const override {
    return PersonalityType::PersCieXyz;
  }
  unsigned short footprint() const override {
    return 6;
  }
  HueColorMode hueColorMode() const override {
    return HueColorModeXyz;
  }

  LightValue hueRepr(const std::vector<uint8_t> &dmx) const override;
};

/**
 * Similar to RGB, but calibrates the results for consistency.
 */
class RgbCalibratedPersonality: public Personality {
 public:
  explicit RgbCalibratedPersonality() = default;

  PersonalityType type() const override {
    return PersonalityType::PersRgbCalibrated;
  }
  HueColorMode hueColorMode() const override {
    return HueColorMode::HueColorModeXyz;
  }
  unsigned short footprint() const override {
    return 8;
  }

  LightValue hueRepr(const std::vector<uint8_t> &dmx) const override;
};

class Light {
 public:
  struct LightInfo {
    std::string modelType;
    std::string modelId;
    std::string manuName;
    std::string productName;
    std::string swVersion;
  };
  explicit Light(const LightId &id,
                 const std::string &name,
                 const unsigned int &address = 1,
                 PersonalityType personalityType = PersonalityType::PersNone,
                 LightInfo info = LightInfo());

  Light(const Light &light) : Light(light.getLightId(),
                                    light.getName(),
                                    light.getAddress(),
                                    light.getPersonalityType(),
                                    light.info) {}

  LightId getLightId() const { return m_id; }
  std::string getName() const { return m_name; }
  void setName(const std::string &name) { m_name = name; }
  unsigned int getAddress() const { return m_address; }
  void setAddress(const unsigned int &address) { m_address = address; }
  PersonalityType getPersonalityType() const { return m_personality->type(); }
  void setPersonalityType(const PersonalityType &personality);
  Personality *getPersonality() { return m_personality.get(); };
  void setInfoFromJson(const web::json::object &lightData);

  LightInfo info;

 private:
  LightId m_id;
  unsigned int m_address;
  std::unique_ptr<Personality> m_personality;
  std::string m_name;
};

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif //PLUGINS_HUE_LIB_LIGHT_H_
