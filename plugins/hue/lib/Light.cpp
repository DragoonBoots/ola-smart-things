//
// Created by dankeenan on 5/17/20.
//

#include "Light.h"

#include <utility>

namespace ola {
namespace plugin {
namespace hue {
namespace lib {
std::unordered_map<std::string, PersonalityType> Personality::nameToTypeMap() {
  static std::unordered_map<std::string, PersonalityType> name_type_map;
  if (name_type_map.empty()) {
    // Reverse the type-to-name map
    const std::unordered_map<PersonalityType, std::string> &type_name_map = typeToNameMap();
    name_type_map.reserve(type_name_map.size());
    for (const std::pair<const PersonalityType, std::string> &item : type_name_map) {
      name_type_map.insert({item.second, item.first});
    }
  }

  return name_type_map;
}
PersonalityType Personality::nameToType(const std::string &name) {
  return nameToTypeMap().at(name);
}
std::unordered_map<PersonalityType, std::string> Personality::typeToNameMap() {
  static std::unordered_map<PersonalityType, std::string> type_name_map(
      {
          {PersonalityType::PersNone, "Not used"},
          {PersonalityType::PersRgbDirect, "RGB (Direct)"},
          {PersonalityType::PersRgbCalibrated, "RGB (Calibrated)"},
          {PersonalityType::PersCieXyz, "CIE XY"},
      }
  );

  return type_name_map;
}
std::string Personality::typeToName(const PersonalityType &personality) {
  return typeToNameMap().at(personality);
}

LightValue RgbDirectPersonality::hueRepr(const std::vector<uint8_t> &dmx) const {
  LightValue value;
  std::copy_n(dmx.begin(), value.size(), value.begin());

  return value;
}

LightValue CieXyzPersonality::hueRepr(const std::vector<uint8_t> &dmx) const {
  LightValue value;
  std::copy_n(dmx.begin(), value.size(), value.begin());

  return value;
}

LightValue RgbCalibratedPersonality::hueRepr(const std::vector<uint8_t> &dmx) const {
  // Convert RGB to XYintensity for better consistency across emitter types.
  // This algorithm comes from https://github.com/mikz/PhilipsHueSDKiOS/blob/master/ApplicationDesignNotes/RGB%20to%20xy%20Color%20conversion.md
  const double R = JOIN_BYTES(dmx[0], dmx[1]);
  const double G = JOIN_BYTES(dmx[2], dmx[3]);
  const double B = JOIN_BYTES(dmx[4], dmx[5]);
  const double r = R / UINT16_MAX;
  const double g = G / UINT16_MAX;
  const double b = B / UINT16_MAX;
  // Gamma correction
  const double r_srgb = (r > 0.04045) ? pow((r + 0.055) / (1.0 + 0.055), 2.4) : (r / 12.92);
  const double g_srgb = (g > 0.04045) ? pow((g + 0.055) / (1.0 + 0.055), 2.4) : (g / 12.92);
  const double b_srgb = (b > 0.04045) ? pow((b + 0.055) / (1.0 + 0.055), 2.4) : (b / 12.92);
  // Matrix math using Wide RGB D65
  const double X = (0.649926 * r_srgb) + (0.103455 * g_srgb) + (0.197109 * b_srgb);
  const double Y = (0.234327 * r_srgb) + (0.743075 * g_srgb) + (0.022598 * b_srgb);
  const double Z = (0.000000 * r_srgb) + (0.053077 * g_srgb) + (1.035763 * b_srgb);
  // Calc XY position on CIE Graph
  const double sum = X + Y + Z;
  // Clamp to specific precision
  const double x = (std::abs(sum) < 0.000001) ? 0 : (X / sum);
  const double y = (std::abs(sum) < 0.000001) ? 0 : (Y / sum);
  const uint16_t x_scaled = x * UINT16_MAX;
  const uint16_t y_scaled = y * UINT16_MAX;

  LightValue value;
  value[0] = BYTE_HI(x_scaled);
  value[1] = BYTE_LO(x_scaled);
  value[2] = BYTE_HI(y_scaled);
  value[3] = BYTE_LO(y_scaled);
  value[4] = dmx[6];
  value[5] = dmx[7];

  return value;
}

Light::Light(const LightId &id,
             const std::string &name,
             const unsigned int &address,
             PersonalityType personalityType,
             LightInfo info) :
    info(info), m_id(id), m_address(address), m_personality(nullptr), m_name(name) {
  setPersonalityType(personalityType);
}

void Light::setPersonalityType(const PersonalityType &personality) {
  switch (personality) {
    case PersonalityType::PersNone:m_personality = std::unique_ptr<Personality>(new NonePersonality());
      break;
    case PersonalityType::PersRgbDirect:m_personality = std::unique_ptr<Personality>(new RgbDirectPersonality());
      break;
    case PersonalityType::PersRgbCalibrated:
      m_personality = std::unique_ptr<Personality>(new RgbCalibratedPersonality());
      break;
    case PersonalityType::PersCieXyz:m_personality = std::unique_ptr<Personality>(new CieXyzPersonality());
      break;
  }
}

void Light::setInfoFromJson(const web::json::object &lightData) {
  if (lightData.find("type") != lightData.cend()) {
    info.modelType = lightData.at("type").as_string();
  }
  if (lightData.find("modelid") != lightData.cend()) {
    info.modelId = lightData.at("modelid").as_string();
  }
  if (lightData.find("manufacturername") != lightData.cend()) {
    info.manuName = lightData.at("manufacturername").as_string();
  }
  if (lightData.find("productname") != lightData.cend()) {
    info.productName = lightData.at("productname").as_string();
  }
  if (lightData.find("swversion") != lightData.cend()) {
    info.swVersion = lightData.at("swversion").as_string();
  }
}

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
