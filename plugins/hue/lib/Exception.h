//
// Created by dankeenan on 5/17/20.
//

#ifndef PLUGINS_HUE_LIB_EXCEPTION_H_
#define PLUGINS_HUE_LIB_EXCEPTION_H_

#include <exception>
#include <string>

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

/**
 * Base exception from this library.
 */
class HueException : public std::runtime_error {
 public:
  explicit HueException(const std::string &message) : std::runtime_error(message) {}
};

/**
 * Generic exception for bridge errors.
 *
 * These almost always indicate a problem outside of this program that may
 * require user intervention, e.g. press the link button, configure a light, etc.
 */
class ResponseException : public HueException {
 public:
  explicit ResponseException(const std::string &message) : HueException(message) {}

};

/**
 * Specific exception when the Bridge config is incorrect.
 */
class BridgeConfigException : public HueException {
 public:
  explicit BridgeConfigException(const std::string &message) : HueException(message) {}
};

/**
 * An exception encountered while trying to stream data.
 */
class StreamException : public HueException {
 public:
  explicit StreamException(const std::string &message) : HueException(message) {}
};

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif //PLUGINS_HUE_LIB_EXCEPTION_H_
