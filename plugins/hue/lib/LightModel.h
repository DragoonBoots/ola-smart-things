//
// Created by dankeenan on 5/19/20.
//

#ifndef PLUGINS_HUE_LIB_LIGHTMODEL_H_
#define PLUGINS_HUE_LIB_LIGHTMODEL_H_

#include <cstdint>

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

/**
 * Supported lights
 *
 * @see https://developers.meethue.com/develop/hue-api/supported-devices/
 */
enum LightModel : uint16_t {
  // Hue bulb A19, Extended Color Light, Color Gamut B
  LCT001 = 1,
  LCT007,
  // Hue bulb A19, Extended Color Light, Color Gamut C
  LCT010 = 11,
  LCT014,
  LCT015,
  LCT016,
  // Hue Sport BR30, Extended Color Light, Color Gamut B
  LCT002 = 21,
  // Hue Sport GU10, Extended Color Light, Color Gamut C
  LCT003 = 31,
  // Hue BR30 Richer Colors , Color Temperature Light, 2200K-6500K
  LTW011 = 41,
  // Hue LightStrips, Color Light, Color Gamut A
  LST001 = 51,
  // Hue Living Colors Iris, Color Light, Color Gamut A
  LLC010 = 61,
  // Hue Living Colors Bloom, Color Light, Color Gamut A
  LLC011 = 71,
  LLC012,
  // Living Colors Gen3 Iris, Color Light, Color Gamut A
  LLC006 = 81,
  // Living Colors Gen3 Bloom/Aura, Color Light, Color Gamut A
  LLC005 = 91,
  LLC007,
  LLC014,
  // Disney Living Colors, Color Light, Color Gamut A
  LLC013 = 101,
  // Hue White, Dimmable Light, No Color
  LWB004 = 111,
  LWB006,
  LWB007,
  // Hue White Lamp, Dimmable Light, No Color
  LWB010 = 121,
  LWB014,
  // Color Light Module, Extended Color Light, Color Gamut B
  LLM001 = 131,
  // Color Temperature Module, Color Temperature Light, 2200K-6500K
  LLM010 = 141,
  LLM011,
  LLM012,
  // Hue A19 White Ambiance, Color Temperature Light, 2200K-6500K
  LTW001 = 151,
  LTW004,
  LTW010,
  LTW015,
  // Hue ambiance spot, Color Temperature Light, 2200K-6500K
  LTW013 = 161,
  LTW014,
  // Hue Go, Extended Color Light, Color Gamut C
  LLC020 = 171,
  // Hue LightStrips Plus, Extended Color Light, Color Gamut C
  LST002 = 181,
  // Hue color candle, Extended Color Light, Color Gamut C
  LCT012 = 191,
  // Hue ambiance candle, Color Temperature Light, 2200K-6500K
  LTW012 = 201,
  // Hue ambiance pendant, Color Temperature Light, 2200K-6500K
  LTP001 = 211,
  LTP002,
  LTP003,
  LTP004,
  LTP005,
  LTD003,
  // Hue ambiance ceiling, Color Temperature Light, 2200K-6500K
  LTF001 = 221,
  LTF002,
  LTC001,
  LTC002,
  LTC003,
  LTC004,
  LTC011,
  LTC012,
  LTD001,
  LTD002,
  // Hue ambiance floor, Color Temperature Light, 2200K-6500K
  LFF001 = 231,
  // Hue ambiance table, Color Temperature Light, 2200K-6500K
  LTT001 = 241,
  // Hue ambiance downlight, Color Temperature Light, 2200K-6500K
  LDT001 = 251,
  // Hue white wall washer, Dimmable Light, No Color
  LDF002 = 261,
  // Hue white ceiling, Dimmable Light, No Color
  LDF001 = 271,
  // Hue white floor, Dimmable Light, No Color
  LDD002 = 281,
  // Hue white table, Dimmable Light, No Color
  LDD001 = 291,
  // Hue white 1-10V, Dimmable Light, No Color
  MWM001 = 301,
};

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif //PLUGINS_HUE_LIB_LIGHTMODEL_H_
