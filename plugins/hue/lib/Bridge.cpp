//
// Created by dankeenan on 5/16/20.
//

#include <iostream>
#include <utility>
#include <cpprest/base_uri.h>
#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <cpprest/uri_builder.h>
#include "Bridge.h"
#include "Exception.h"

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace web::json;

const char Bridge::APP_NAME[] = "olasmartthings";

Bridge::Bridge(const std::string &address,
               const std::string &deviceName,
               const std::string &username,
               const std::string &clientkey)
    :
    m_address(address),
    m_username(username),
    m_client_key(clientkey),
    m_device_name(deviceName),
    m_client(U("http://" + address + "/api")) {
}

uri_builder Bridge::getUriBuilder(const std::string &endpoint) {
  return uri_builder(U("/" + m_username + endpoint));
}

json::value Bridge::blockingBridgeComm(const pplx::task<http_response> &requestTask) {
  const http_response &resp = requestTask.get();

  return handleBridgeResp(resp);
}

void Bridge::asyncBridgeComm(const pplx::task<http_response> &requestTask,
                             const std::function<void(json::value)> &callback) {
  if (callback) {
    requestTask.then([&callback](const http_response &resp) {
      callback(handleBridgeResp(resp));
    });
  }
}

json::value Bridge::handleBridgeResp(const http_response &resp) {
  if (resp.status_code() != status_codes::OK) {
    throw BridgeConfigException("Error " + std::to_string(resp.status_code()) + " from bridge." +
        "  Check the bridge configuration is correct and the bridge is online.");
  }
  json::value data;
  try {
    data = resp.extract_json().get();
  } catch (json::json_exception &e) {
    throw ResponseException("Bad response from bridge: " + std::string(e.what()));
  }

  return data;
}

bool Bridge::isRegistered() {
  if (m_username.empty() || m_client_key.empty()) {
    // Definitely not registered
    return false;
  }

  const uri_builder endpoint = getUriBuilder("/config");
  const json::value resp = blockingBridgeComm(m_client.request(methods::GET, endpoint.to_string()));

  // Only registered users can see this field.
  return resp.has_field("whitelist");
}

Bridge::Auth Bridge::authRegister() {
  if (isRegistered()) {
    // No need to re-register
    return Auth{m_username, m_client_key};
  }

  // Register the user and save the result.
  json::value body = json::value::object();
  body["devicetype"] = json::value::string(std::string(APP_NAME) + "#" + m_device_name);
  body["generateclientkey"] = json::value::boolean(true);
  const json::value resp = blockingBridgeComm(m_client.request(methods::POST, "/", body)).as_array().at(0);
  if (resp.has_object_field("error")) {
    // The most likely error is the link button not being pressed.
    const json::value &error = resp.at("error");
    const std::string &message = error.at("description").as_string();
    throw ResponseException("Error registering with bridge: " + message);
  } else if (resp.has_object_field("success")) {
    // Got the username and client key
    const json::value &success = resp.at("success");
    m_username = success.at("username").as_string();
    m_client_key = success.at("clientkey").as_string();
    return Auth{m_username, m_client_key};
  }

  throw ResponseException("Bad response from bridge");
}

GroupId Bridge::getGroupId(const std::string &deviceId) {
  // Find the control group, if it exists.
  const std::string group_name = std::string(APP_NAME) + " (" + deviceId + ")";
  const uri_builder endpoint = getUriBuilder("/groups");
  const json::object resp = blockingBridgeComm(m_client.request(methods::GET, endpoint.to_string())).as_object();
  GroupId group_id;
  for (const std::pair<std::string, json::value> &item : resp) {
    group_id = std::stoul(item.first);
    const json::object value = item.second.as_object();
    if (value.at("name").as_string() == group_name) {
      return group_id;
    }
  }

  // The group needs to be created.
  return createGroup(group_name);
}

GroupId Bridge::createGroup(const std::string &groupName) {
  const uri_builder endpoint = getUriBuilder("/groups");
  json::value body = json::value::object();
  body["lights"] = json::value::array();
  body["name"] = json::value::string(groupName);
  body["type"] = json::value::string("Entertainment");
  body["class"] = json::value::string("Other");
  const json::value resp =
      blockingBridgeComm(m_client.request(methods::POST, endpoint.to_string(), body)).as_array().at(0);
  if (resp.has_object_field("error")) {
    const json::value &error = resp.at("error");
    const std::string &message = error.at("description").as_string();
    throw ResponseException("Error creating light group: " + message);
  } else if (resp.has_object_field("success")) {
    // Got the new group id
    const json::value &success = resp.at("success");
    const GroupId group_id = std::stoul(success.at("id").as_string());

    return group_id;
  }

  throw ResponseException("Bad response from bridge");
}

LightList Bridge::getLights() {
  LightList lights;
  const uri_builder endpoint = getUriBuilder("/lights");
  const json::object resp = blockingBridgeComm(m_client.request(methods::GET, endpoint.to_string())).as_object();
  for (const json::object::iterator::value_type &item : resp) {
    const LightId light_id = std::stoul(item.first);
    const json::object value = item.second.as_object();
    const std::string name = value.at("name").as_string();
    Light light(light_id, name);
    light.setInfoFromJson(value);
    lights.insert({light_id, std::move(light)});
  }

  return lights;
}

void Bridge::setGroupLights(const GroupId &groupId, const LightList &lights) {
  // Build the list of lights to add
  std::vector<json::value> light_ids;
  for (const LightList::value_type &value : lights) {
    light_ids.push_back(json::value::string(std::to_string(value.first)));
  }

  // Build the request
  const uri_builder endpoint = getUriBuilder("/groups/" + std::to_string(groupId));
  json::value body = json::value::object();
  body["lights"] = json::value::array(light_ids);
  const json::array resp = blockingBridgeComm(m_client.request(methods::PUT, endpoint.to_string(), body)).as_array();
  // Verify this was successful.
  for (const json::array::iterator::value_type &value : resp) {
    if (value.has_field("error")) {
      const json::value &error = value.at("error");
      const std::string &message = error.at("description").as_string();
      throw ResponseException("Error adding lights to control group: " + message);
    }
  }
}

void Bridge::setName(const Light &light) {
  const uri_builder endpoint = getUriBuilder("/lights/" + std::to_string(light.getLightId()));
  json::value body = json::value::object();
  body["name"] = json::value::string(light.getName());
  asyncBridgeComm(m_client.request(methods::PUT, endpoint.to_string(), body));
}

void Bridge::setIdentify(const Light &light, const bool &state) {
  const uri_builder endpoint = getUriBuilder("/lights/" + std::to_string(light.getLightId()) + "/state");
  json::value body = json::value::object();
  body["alert"] = json::value::string(state ? "lselect" : "none");
  asyncBridgeComm(m_client.request(methods::PUT, endpoint.to_string(), body));
}

void Bridge::setStreaming(const GroupId &groupId, const bool &state) {
  const uri_builder endpoint = getUriBuilder("/groups/" + std::to_string(groupId));
  json::value body = json::value::object();
  body["stream"] = json::value::object();
  body["stream"]["active"] = json::value::boolean(state);
  const json::array resp = blockingBridgeComm(m_client.request(methods::PUT, endpoint.to_string(), body)).as_array();
  // Verify this was successful.
  for (const json::array::iterator::value_type &value : resp) {
    if (value.has_field("error")) {
      const json::value &error = value.at("error");
      const std::string &message = error.at("description").as_string();
      throw ResponseException("Error enabling streaming: " + message);
    }
  }
}

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
