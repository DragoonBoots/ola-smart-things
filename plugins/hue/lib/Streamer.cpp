//
// Created by dankeenan on 5/19/20.
//

#include <gnutls/dtls.h>
#include "Streamer.h"
#include "Exception.h"

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

Streamer::Streamer(std::shared_ptr<Bridge> bridge, const GroupId &groupId) :
    m_bridge(bridge), m_group_id(groupId) {
}

Streamer::~Streamer() {
  stop();
}

void Streamer::start() {
  m_bridge->setStreaming(m_group_id, true);
  m_dtls_session_opening = true;

  // Init the client
  int ret = 0;
  handleGnutlsError(gnutls_init(&m_dtls_session, GNUTLS_CLIENT | GNUTLS_DATAGRAM | GNUTLS_AUTO_REAUTH),
                    "while initializing the session for streaming");
  {
    gnutls_global_set_log_function(&Streamer::gnutlsLog);
    gnutls_global_set_log_level(1);
    const char *errpos = nullptr;
//    ret = gnutls_set_default_priority(m_dtls_session);
//    ret = gnutls_set_default_priority_append(m_dtls_session, "PERFORMANCE:+VERS-DTLS1.2:+PSK:+AES-128-GCM:+SHA256", &errpos, 0);
    ret = gnutls_priority_set_direct(m_dtls_session, "PERFORMANCE:+VERS-DTLS1.2:+PSK:+AES-128-GCM:+SHA256", &errpos);
    if (ret < 0) {
      if (ret == GNUTLS_E_INVALID_REQUEST) {
        throw StreamException("Bad cipher string at: " + std::string(errpos));
      } else {
        handleGnutlsError(ret, "while setting allowed ciphers for streaming");
      }
    }
  }
//  gnutls_dtls_set_mtu(m_dtls_session, BRIDGE_MTU);
//  gnutls_dtls_set_timeouts(m_dtls_session, 1000, BRIDGE_HANDSHAKE_TIMEOUT);
  gnutls_handshake_set_timeout(m_dtls_session, BRIDGE_HANDSHAKE_TIMEOUT);
//  gnutls_transport_set_push_function(m_dtls_session, &Streamer::dtlsPush);
//  gnutls_session_set_keylog_function(m_dtls_session, &Streamer::gnutlsKeyLog);
  // Set credentials
  handleGnutlsError(gnutls_psk_allocate_client_credentials(&m_dtls_credentials),
                    "while initializing credentials for streaming");
  const Bridge::Auth &bridge_auth = m_bridge->getAuth();
  const gnutls_datum_t
      key =
      {(unsigned char *) bridge_auth.clientkey.c_str(), static_cast<unsigned int>(bridge_auth.clientkey.size())};
  handleGnutlsError(gnutls_psk_set_client_credentials(m_dtls_credentials,
                                                      bridge_auth.username.c_str(),
                                                      &key,
                                                      GNUTLS_PSK_KEY_HEX),
                    "while creating credentials for streaming");
  handleGnutlsError(gnutls_credentials_set(m_dtls_session, GNUTLS_CRD_PSK, m_dtls_credentials),
                    "while setting credentials for streaming");
  // Create socket
  m_dtls_socket = udpConnect(m_bridge->getAddress(), BRIDGE_PORT);
  gnutls_transport_set_int(m_dtls_session, m_dtls_socket);
  // Handshake
  // Keep trying to handshake until done or fatal error.
  for (;;) {
    ret = gnutls_handshake(m_dtls_session);
    if (ret < 0) {
      if (gnutls_error_is_fatal(ret)) {
        handleGnutlsError(ret, "during handshaking for streaming");
      } else {
        continue;
      }
    } else {
      break;
    }
  }
  // Sometimes the bridge forgets about streaming while opening the DTLS connection...
  m_bridge->setStreaming(m_group_id, true);
  m_dtls_session_open = true;
  m_dtls_session_opening = false;
}

void Streamer::stop() {
  if (m_dtls_session != nullptr) {
    if (m_dtls_session_open) {
      gnutls_bye(m_dtls_session, GNUTLS_SHUT_WR);
      m_dtls_session_open = false;
    }
    gnutls_deinit(m_dtls_session);
    m_dtls_session = nullptr;
  }
  if (m_dtls_socket != 0) {
    close(m_dtls_socket);
    m_dtls_socket = 0;
  }
  if (m_dtls_credentials != nullptr) {
    gnutls_psk_free_client_credentials(m_dtls_credentials);
    m_dtls_credentials = nullptr;
  }
  m_bridge->setStreaming(m_group_id, false);
}

void Streamer::handleGnutlsError(int error_code, const std::string &) {
  if (error_code < 0) {
    const bool is_alert_warning = (error_code == GNUTLS_E_WARNING_ALERT_RECEIVED);
    const bool is_alert_error = (error_code == GNUTLS_E_FATAL_ALERT_RECEIVED);
    if (is_alert_warning || is_alert_error) {
      gnutls_alert_description_t alert = gnutls_alert_get(m_dtls_session);
      std::string alert_name(gnutls_alert_get_name(alert));
      fputs(("Streaming alert from bridge: " + alert_name + "\n").c_str(), stderr);
    }
    if (is_alert_error || gnutls_error_is_fatal(error_code)) {
//      if (m_dtls_session_opening) {
//        // Failed while opening connection, no recovery.
//        stop();
//        // Standard error code
//        std::string name(gnutls_strerror_name(error_code));
//        std::string message(gnutls_strerror(error_code));
//        throw StreamException("Error " + name + " " + where + ": " + message);
//      } else {
      // Try to reset connection
      stop();
      start();
//      }
    }
  }
}

void Streamer::gnutlsLog(int, const char *message) {
  fputs((std::string(message) + "\n").c_str(), stderr);
}

int Streamer::gnutlsKeyLog(gnutls_session_t, const char *label, const gnutls_datum_t *secret) {
  char secret_hex[secret->size * 2];
  size_t secret_hex_size;
  gnutls_hex_encode(secret, secret_hex, &secret_hex_size);
  fputs((std::string(label) + ": " + std::string(secret_hex, secret_hex_size) + "\n").c_str(), stderr);

  return 0;
}

int Streamer::udpConnect(const std::string &addr, const unsigned short &port) {
  int socket_handle = socket(AF_INET, SOCK_DGRAM, 0);
  struct sockaddr_in socket_addr;
  memset(&socket_addr, 0, sizeof(socket_addr));

  // Where does this socket point
  socket_addr.sin_family = AF_INET;
  socket_addr.sin_port = htons(port);
  inet_pton(socket_addr.sin_family, addr.c_str(), &socket_addr.sin_addr);

  // Options
  int optval;
#if defined(IP_DONTFRAG)
  optval = 1;
  setsockopt(socket_handle, IPPROTO_IP, IP_DONTFRAG, (const void *) &optval, sizeof(optval));
#elif defined(IP_MTU_DISCOVER)
  optval = IP_PMTUDISC_DO;
  setsockopt(socket_handle, IPPROTO_IP, IP_MTU_DISCOVER, (const void *) &optval, sizeof(optval));
#endif
  int err = connect(socket_handle, (struct sockaddr *) &socket_addr, sizeof(socket_addr));
  if (err < 0) {
    throw StreamException("Error " + std::to_string(err) + " while creating socket for streaming.");
  }

  return socket_handle;
}

void Streamer::write(const StreamMessage &message) {
  int ret = gnutls_record_send(m_dtls_session, &message, getMessageSize(message));
  if (ret < 0 && gnutls_error_is_fatal(ret)) {
    handleGnutlsError(ret, "while streaming DMX data");
  }
}
size_t Streamer::getMessageSize(const Streamer::StreamMessage &message) {
  // Start with size of header
  int size = 16;
  for (unsigned int i = 0; i < MAX_SLOTS; i++) {
    const StreamSlot &slot = message.data[i];
    if (slot.lightId == 0) {
      break;
    } else {
      size += sizeof(slot);
    }
  }
  return size;
}

//long Streamer::dtlsPush(gnutls_transport_ptr_t transport_ptr, const void *buffer, size_t len) {
//  int ret = -1;
//  if (transport_ptr != nullptr) {
//    ret = ::write(m_dtls_socket, buffer, len);
//  }
//  return ret;
//}

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
