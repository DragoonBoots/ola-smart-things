//
// Created by dankeenan on 5/19/20.
//

#ifndef PLUGINS_HUE_LIB_STREAMER_H_
#define PLUGINS_HUE_LIB_STREAMER_H_

#include <cstdint>
#include <gnutls/gnutls.h>
#include "Light.h"
#include "Bridge.h"

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

class Streamer {
 public:
  static const unsigned char MAX_SLOTS = 10;

#pragma pack(1)
  struct StreamSlot {
    uint8_t type = 0;
    uint16_t lightId = 0;
    uint8_t value[6] = {0, 0, 0, 0, 0, 0};
  };
  struct StreamMessage {
    char protocol[9] = {'H', 'u', 'e', 'S', 't', 'r', 'e', 'a', 'm'};
    uint8_t version[2] = {1, 0};  // Version 1.0
    uint8_t sequenceId = 0; // Bridge says unused, we fill this in.
    uint8_t reserved0[2] = {0, 0};
    HueColorMode colorMode = HueColorMode::HueColorModeRgb;
    uint8_t reserved2 = 0;
    StreamSlot data[MAX_SLOTS];
  };
#pragma pack()

  explicit Streamer(std::shared_ptr<Bridge> bridge, const GroupId &groupId);
  ~Streamer();

  /**
   * Start the streaming process.
   */
  void start();

  /**
   * Stop streaming data and breakdown the connection.
   */
  void stop();

  /**
   * Write messages
   * @param message
   */
  void write(const StreamMessage &message);

 private:
  /**
   * The port the bridge uses for DTLS communication.
   */
  const unsigned short BRIDGE_PORT = 2100;
  const unsigned short BRIDGE_MTU = 1000;
  const unsigned short BRIDGE_HANDSHAKE_TIMEOUT = 10000;

  std::shared_ptr<Bridge> m_bridge;
  GroupId m_group_id;

  bool m_dtls_session_open = false;
  bool m_dtls_session_opening = false;
  gnutls_session_t m_dtls_session = nullptr;
  int m_dtls_socket;
  gnutls_psk_client_credentials_t m_dtls_credentials = nullptr;

  size_t getMessageSize(const StreamMessage &message);

  void handleGnutlsError(int error_code, const std::string &where);
  static void gnutlsLog(int level, const char *message);
  static int gnutlsKeyLog(gnutls_session_t session,
                          const char *label,
                          const gnutls_datum_t *secret);
  int udpConnect(const std::string &addr, const unsigned short &port);
//  static long dtlsPush(gnutls_transport_ptr_t transport_ptr, const void *buffer, size_t len);
//  static void *dtlsTransportPtr() { return &m_dtls_socket; }
};

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */


#endif //PLUGINS_HUE_LIB_STREAMER_H_
