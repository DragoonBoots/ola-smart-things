//
// Created by dankeenan on 5/16/20.
//

#ifndef PLUGINS_HUE_LIB_BRIDGE_H_
#define PLUGINS_HUE_LIB_BRIDGE_H_

#include <string>
#include <cpprest/http_client.h>
#include <cpprest/http_msg.h>
#include "Light.h"

namespace ola {
namespace plugin {
namespace hue {
namespace lib {

using namespace utility;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace web::json;

typedef std::unordered_map<LightId, Light> LightList;

/**
 * Manage communication with a Hue Bridge
 */
class Bridge {
 public:
  struct Auth {
    std::string username;
    std::string clientkey;
  };

  Bridge(const std::string &address,
         const std::string &deviceName,
         const std::string &username = "",
         const std::string &clientkey = "");

  Auth getAuth() { return Auth{m_username, m_client_key}; }
  std::string getAddress() { return m_address; }

  /**
   * Register this program with the bridge
   *
   * If this program is already registered, no action is taken.
   *
   * @return The authentication information.
   * @throws ResponseException When the bridge responds badly.
   * @throws BridgeConfigException When the bridge does not exist.
   */
  Auth authRegister();

  /**
   * Get the group id used for control.
   *
   * If the group does not exist, it is created.
   * @return
   */
  GroupId getGroupId(const std::string &deviceId);

  /**
   * Get all of the lights this bridge controls.
   * @return The lights, keyed by light id.
   */
  LightList getLights();

  /**
   * Add lights to the group.
   * @param groupId
   * @param lights
   */
  void setGroupLights(const GroupId &groupId, const LightList &lights);

  /**
   * Set light name.
   * @param light
   * @param name
   */
  void setName(const Light &light);

  /**
   * Set the identify ("alert" state).
   * @param state
   */
  void setIdentify(const Light &light, const bool &state);

  /**
   * Enable/disable streaming.
   * @param groupId
   * @param state
   */
  void setStreaming(const GroupId &groupId, const bool &state);

 private:
  static const char APP_NAME[];
  std::string m_address;
  std::string m_username = "";
  std::string m_client_key = "";
  std::string m_device_name;
  http_client m_client;

  /**
   * Prepare a URI builder from the given endpoint
   * @param endpoint
   * @return
   */
  uri_builder getUriBuilder(const std::string &endpoint);

  /**
   * Handle all communication with the Hue Bridge in a blocking manner.
   *
   * @param requestTask A task created with web::http:client::http_client::request()
   * @return The JSON response from the request
   * @throws ResponseException When the bridge responds with malformed JSON.
   * @throws BridgeConfigException When the bridge does not exist.
   */
  static json::value blockingBridgeComm(const pplx::task<http_response> &requestTask);

  /**
   * Handle all communication with the Hue Bridge in an async manner.
   *
   * @param requestTask
   * @throws ResponseException When the bridge responds with malformed JSON.
   * @throws BridgeConfigException When the bridge does not exist.
   */
  static void asyncBridgeComm(const pplx::task<http_response> &requestTask,
                              const std::function<void(json::value)> &callback = std::function<void(json::value)>());

  /**
   * Verify and parse the bridge response.
   * @param resp
   * @return
   */
  static json::value handleBridgeResp(const http_response &resp);

  /**
   * Determine if the config has a valid username.
   * @return
   */
  bool isRegistered();

  /**
   * Create the special control group
   * @return
   */
  GroupId createGroup(const std::string &groupName);
};

} /* namespace lib */
} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif /* PLUGINS_HUE_LIB_BRIDGE_H_ */
