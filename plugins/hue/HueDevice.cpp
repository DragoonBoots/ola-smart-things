//
// Created by dankeenan on 5/17/20.
//

#include <include/ola/Logging.h>
#include "lib/Exception.h"
#include "lib/Light.h"
#include "HueDevice.h"

namespace ola {
namespace plugin {
namespace hue {
const char HueDevice::DEVICE_NAME[] = "Hue Bridge";

bool HueDevice::StartHook() {
  // Initialize the bridge
  m_group_id = m_bridge->getGroupId(DeviceId());
  lib::LightList patch_lights;
  lib::LightList use_lights;
  lib::LightList bridge_lights;
  try {
    bridge_lights = m_bridge->getLights();
    patch_lights = m_showfile->getPatch();
    if (patch_lights.empty()) {
      // Initialize the patch with the bridge's available lights.
      m_showfile->setPatch(bridge_lights);
      patch_lights = bridge_lights;
    }
    // First ensure the patch and the bridge refer to the same lights.
    for (const lib::LightList::value_type &item : bridge_lights) {
      const lib::LightId &light_id = item.first;
      const lib::Light &light = item.second;
      if (patch_lights.find(light_id) == patch_lights.end()) {
        patch_lights.insert({light_id, light});
        m_showfile->setPatch(light);
      }
    }
    // Use lights from the patch with a personality set.
    for (lib::LightList::value_type &item : patch_lights) {
      const lib::LightId &light_id = item.first;
      lib::Light &light = item.second;
      if (bridge_lights.find(light_id) == bridge_lights.end()) {
        OLA_WARN << "Light id " + std::to_string(light_id)
            + " is in the config but is not known to the bridge.  This is an error.";
        return false;
      }
      // Take the meta info from the bridge
      light.info = bridge_lights.at(light_id).info;
      if (light.getPersonalityType() != lib::PersonalityType::PersNone) {
        use_lights.insert({light_id, light});
      }
    }
    if (use_lights.empty()) {
      // The bridge doesn't support an empty group, so use all of the lights.
      m_bridge->setGroupLights(m_group_id, patch_lights);
    } else {
      m_bridge->setGroupLights(m_group_id, use_lights);
    }
  } catch (lib::ResponseException &e) {
    OLA_WARN << e.what();
    return false;
  }

  // Build the list of lights for the port.
  std::vector<std::shared_ptr<lib::Light>> light_list;
  for (const lib::LightList::value_type &item : patch_lights) {
//    const lib::LightId &light_id = item.first;
    const lib::Light &light = item.second;
    light_list.push_back(std::make_shared<lib::Light>(light));
  }

  // Setup the output port
  try {
    m_port = new HueOutputPort(this, m_bridge, light_list, m_showfile, m_group_id);

    if (!m_port->start() || !AddPort(m_port)) {
      delete m_port;
      return false;
    }
  } catch (lib::StreamException &e) {
    OLA_WARN << e.what();
    return false;
  }
  return true;
}

void HueDevice::PrePortStop() {
  m_port->stop();
}

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
