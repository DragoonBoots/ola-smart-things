//
// Created by dankeenan on 5/18/20.
//

#include "ola/rdm/ResponderHelper.h"
#include "HueRdm.h"
#include "lib/LightModel.h"

namespace ola {
namespace plugin {
namespace hue {

HueResponder::HueResponder(const rdm::UID &uid,
                           const std::shared_ptr<lib::Light> &light,
                           const std::shared_ptr<lib::Bridge> &bridge,
                           const std::shared_ptr<ShowFile> &showFile) :
    m_uid(uid),
    m_bridge(bridge),
    m_light(light),
    m_start_address(light->getAddress()),
    m_show_file(showFile),
    m_identify_mode(false),
    m_personalities(Personalities::get()),
    m_personality_manager(m_personalities) {
  uint8_t active_personality_id = m_personalities->lookupId(light->getPersonalityType());
  m_personality_manager.SetActivePersonality(active_personality_id);
}

void HueResponder::SendRDMRequest(rdm::RDMRequest *request, rdm::RDMCallback *callback) {
  RdmOps::get()->HandleRDMRequest(this, m_uid, rdm::ROOT_RDM_DEVICE, request, callback);
}

const rdm::ResponderOps<HueResponder>::ParamHandler HueResponder::PARAM_HANDLERS[]{
    {rdm::PID_DEVICE_INFO, &HueResponder::getDeviceInfo, nullptr},
    {rdm::PID_SOFTWARE_VERSION_LABEL, &HueResponder::getSoftwareVersionLabel, nullptr},
    {rdm::PID_DMX_START_ADDRESS, &HueResponder::getAddress, &HueResponder::setAddress},
    {rdm::PID_DMX_PERSONALITY, &HueResponder::getPersonality, &HueResponder::setPersonality},
    {rdm::PID_DMX_PERSONALITY_DESCRIPTION, &HueResponder::getPersonalityLabel, nullptr},
    {rdm::PID_IDENTIFY_DEVICE, &HueResponder::getIdentify, &HueResponder::setIdentify},
    {rdm::PID_MANUFACTURER_LABEL, &HueResponder::getManufacturerLabel, nullptr},
    {rdm::PID_DEVICE_LABEL, &HueResponder::getDeviceLabel, &HueResponder::setDeviceLabel},
    {rdm::PID_DEVICE_MODEL_DESCRIPTION, &HueResponder::getDeviceModelDescription, nullptr},
    {0, nullptr, nullptr}
};

rdm::RDMResponse *HueResponder::getDeviceInfo(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetDeviceInfo(
      request,
      deviceIdMap().at(m_light->info.modelId),
      rdm::PRODUCT_CATEGORY_FIXTURE_FIXED,
      createVersionNumber(),
      &m_personality_manager,
      m_start_address,
      0, 0
  );
}
rdm::RDMResponse *HueResponder::getSoftwareVersionLabel(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetString(request, m_light->info.swVersion);
}
rdm::RDMResponse *HueResponder::getAddress(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetDmxAddress(request, &m_personality_manager, m_start_address);
}
rdm::RDMResponse *HueResponder::setAddress(const rdm::RDMRequest *request) {
  uint16_t old_address = m_start_address;
  rdm::RDMResponse *resp = rdm::ResponderHelper::SetDmxAddress(request, &m_personality_manager, &m_start_address);
  if (m_start_address != old_address) {
    // Address changed successfully, update containers
    m_light->setAddress(m_start_address);
    m_show_file->setPatch(*m_light);
  }

  return resp;
}
rdm::RDMResponse *HueResponder::getPersonality(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetPersonality(request, &m_personality_manager);
}
rdm::RDMResponse *HueResponder::setPersonality(const rdm::RDMRequest *request) {
  lib::PersonalityType
      old_personality = lib::Personality::nameToType(m_personality_manager.ActivePersonality()->Description());
  rdm::RDMResponse *resp = rdm::ResponderHelper::SetPersonality(request, &m_personality_manager, m_start_address);
  const lib::PersonalityType
      new_personality = lib::Personality::nameToType(m_personality_manager.ActivePersonality()->Description());
  if (old_personality != new_personality) {
    // Personality changed successfully
    m_light->setPersonalityType(new_personality);
    m_show_file->setPatch(*m_light);
  }

  return resp;
}
rdm::RDMResponse *HueResponder::getPersonalityLabel(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetPersonalityDescription(request, &m_personality_manager);
}
rdm::RDMResponse *HueResponder::getIdentify(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetBoolValue(request, m_identify_mode);
}
rdm::RDMResponse *HueResponder::setIdentify(const rdm::RDMRequest *request) {
  bool old_value = m_identify_mode;
  rdm::RDMResponse *resp = rdm::ResponderHelper::SetBoolValue(request, &m_identify_mode);
  if (m_identify_mode != old_value) {
    // Identify changed successfully
    m_bridge->setIdentify(*m_light, m_identify_mode);
  }

  return resp;
}
rdm::RDMResponse *HueResponder::getManufacturerLabel(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetString(request, m_light->info.manuName);
}
rdm::RDMResponse *HueResponder::getDeviceLabel(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetString(request, m_light->getName());
}
rdm::RDMResponse *HueResponder::setDeviceLabel(const rdm::RDMRequest *request) {
  std::string old_label = m_light->getName();
  std::string new_label;
  rdm::RDMResponse *resp = rdm::ResponderHelper::SetString(request, &new_label);
  if (old_label != new_label) {
    // Label changed successfully
    m_light->setName(new_label);
    m_bridge->setName(*m_light);
  }

  return resp;
}
rdm::RDMResponse *HueResponder::getDeviceModelDescription(const rdm::RDMRequest *request) {
  return rdm::ResponderHelper::GetString(request, m_light->info.productName);
}

HueResponder::Personalities *HueResponder::Personalities::get() {
  static HueResponder::Personalities *instance = nullptr;
  if (!instance) {
    const PersonalityInfo pers_info_none = persInfoNone();
    const PersonalityInfo pers_info_rgb_direct = persInfoRgbDirect();
    const PersonalityInfo pers_info_rgb_calibrated = persInfoRgbCalibrated();
    const PersonalityInfo pers_info_cie_xyz = persInfoCieXyz();

    const PersonalityList personalities(
        {
            // None
            rdm::Personality(pers_info_none.footprint,
                             pers_info_none.name,
                             rdm::SlotDataCollection(pers_info_none.slot_data)),
            // RGB (Direct)
            rdm::Personality(pers_info_rgb_direct.footprint,
                             pers_info_rgb_direct.name,
                             rdm::SlotDataCollection(pers_info_rgb_direct.slot_data)),
            // RGB (Calibrated)
            rdm::Personality(pers_info_rgb_calibrated.footprint,
                             pers_info_rgb_calibrated.name,
                             rdm::SlotDataCollection(pers_info_rgb_calibrated.slot_data)),
            // CIE XYy
            rdm::Personality(pers_info_cie_xyz.footprint,
                             pers_info_cie_xyz.name,
                             rdm::SlotDataCollection(pers_info_cie_xyz.slot_data))
        }
    );
    instance = new Personalities(personalities);
  }
  return instance;
}

HueResponder::Personalities::PersonalityInfo HueResponder::Personalities::persInfoNone() {
  const rdm::SlotDataCollection::SlotDataList slot_data({});

  // This personality uses no slots.
  return PersonalityInfo{0, slot_data, lib::Personality::typeToName(lib::PersonalityType::PersNone)};
}

HueResponder::Personalities::PersonalityInfo HueResponder::Personalities::persInfoRgbDirect() {
  const rdm::SlotDataCollection::SlotDataList slot_data(
      {
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_ADD_RED, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 0, 0),
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_ADD_GREEN, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 2, 0),
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_ADD_BLUE, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 4, 0),
      }
  );

  return PersonalityInfo{6, slot_data, lib::Personality::typeToName(lib::PersonalityType::PersRgbDirect)};
}

HueResponder::Personalities::PersonalityInfo HueResponder::Personalities::persInfoRgbCalibrated() {
  const rdm::SlotDataCollection::SlotDataList slot_data(
      {
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_ADD_RED, 255),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 0, 255),
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_ADD_GREEN, 255),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 2, 255),
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_ADD_BLUE, 255),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 4, 0),
          rdm::SlotData::PrimarySlot(rdm::SD_INTENSITY, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 6, 0),
      }
  );

  return PersonalityInfo{8, slot_data, lib::Personality::typeToName(lib::PersonalityType::PersRgbCalibrated)};
}

HueResponder::Personalities::PersonalityInfo HueResponder::Personalities::persInfoCieXyz() {
  const rdm::SlotDataCollection::SlotDataList slot_data(
      {
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_HUE, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 0, 0),
          rdm::SlotData::PrimarySlot(rdm::SD_COLOR_SATURATION, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 2, 0),
          rdm::SlotData::PrimarySlot(rdm::SD_INTENSITY, 0),
          rdm::SlotData::SecondarySlot(rdm::ST_SEC_FINE, 4, 0),
      }
  );

  return PersonalityInfo{6, slot_data, lib::Personality::typeToName(lib::PersonalityType::PersCieXyz)};
}

const rdm::Personality *HueResponder::Personalities::lookup(lib::PersonalityType personality) const {
  const std::string search_name = lib::Personality::typeToName(personality);
  for (const rdm::Personality &check : m_personalities) {
    if (check.Description() == search_name) {
      return &check;
    }
  }
  return nullptr;
}
uint8_t HueResponder::Personalities::lookupId(lib::PersonalityType personality) const {
  const std::string search_name = lib::Personality::typeToName(personality);
  // The ola personality manager offsets personality ids by 1.
  uint8_t i = 1;
  for (const rdm::Personality &check : m_personalities) {
    if (check.Description() == search_name) {
      return i;
    }
    i++;
  }
  return 0;
}

#define MAP_DEVICE_ID(model_type) {#model_type, lib::LightModel::model_type}
std::unordered_map<std::string, uint16_t> HueResponder::deviceIdMap() {
  static std::unordered_map<std::string, uint16_t> device_id_map(
      {
          // Hue bulb A19, Extended Color Light, Color Gamut B
          MAP_DEVICE_ID(LCT001),
          MAP_DEVICE_ID(LCT007),
          // Hue bulb A19, Extended Color Light, Color Gamut C
          MAP_DEVICE_ID(LCT010),
          MAP_DEVICE_ID(LCT014),
          MAP_DEVICE_ID(LCT015),
          MAP_DEVICE_ID(LCT016),
          // Hue Sport BR30, Extended Color Light, Color Gamut B
          MAP_DEVICE_ID(LCT002),
          // Hue Sport GU10, Extended Color Light, Color Gamut C
          MAP_DEVICE_ID(LCT003),
          // Hue BR30 Richer Colors , Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTW011),
          // Hue LightStrips, Color Light, Color Gamut A
          MAP_DEVICE_ID(LST001),
          // Hue Living Colors Iris, Color Light, Color Gamut A
          MAP_DEVICE_ID(LLC010),
          // Hue Living Colors Bloom, Color Light, Color Gamut A
          MAP_DEVICE_ID(LLC011),
          MAP_DEVICE_ID(LLC012),
          // Living Colors Gen3 Iris, Color Light, Color Gamut A
          MAP_DEVICE_ID(LLC006),
          // Living Colors Gen3 Bloom/Aura, Color Light, Color Gamut A
          MAP_DEVICE_ID(LLC005),
          MAP_DEVICE_ID(LLC007),
          MAP_DEVICE_ID(LLC014),
          // Disney Living Colors, Color Light, Color Gamut A
          MAP_DEVICE_ID(LLC013),
          // Hue White, Dimmable Light, No Color
          MAP_DEVICE_ID(LWB004),
          MAP_DEVICE_ID(LWB006),
          MAP_DEVICE_ID(LWB007),
          // Hue White Lamp, Dimmable Light, No Color
          MAP_DEVICE_ID(LWB010),
          MAP_DEVICE_ID(LWB014),
          // Color Light Module, Extended Color Light, Color Gamut B
          MAP_DEVICE_ID(LLM001),
          // Color Temperature Module, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LLM010),
          MAP_DEVICE_ID(LLM011),
          MAP_DEVICE_ID(LLM012),
          // Hue A19 White Ambiance, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTW001),
          MAP_DEVICE_ID(LTW004),
          MAP_DEVICE_ID(LTW010),
          MAP_DEVICE_ID(LTW015),
          // Hue ambiance spot, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTW013),
          MAP_DEVICE_ID(LTW014),
          // Hue Go, Extended Color Light, Color Gamut C
          MAP_DEVICE_ID(LLC020),
          // Hue LightStrips Plus, Extended Color Light, Color Gamut C
          MAP_DEVICE_ID(LST002),
          // Hue color candle, Extended Color Light, Color Gamut C
          MAP_DEVICE_ID(LCT012),
          // Hue ambiance candle, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTW012),
          // Hue ambiance pendant, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTP001),
          MAP_DEVICE_ID(LTP002),
          MAP_DEVICE_ID(LTP003),
          MAP_DEVICE_ID(LTP004),
          MAP_DEVICE_ID(LTP005),
          MAP_DEVICE_ID(LTD003),
          // Hue ambiance ceiling, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTF001),
          MAP_DEVICE_ID(LTF002),
          MAP_DEVICE_ID(LTC001),
          MAP_DEVICE_ID(LTC002),
          MAP_DEVICE_ID(LTC003),
          MAP_DEVICE_ID(LTC004),
          MAP_DEVICE_ID(LTC011),
          MAP_DEVICE_ID(LTC012),
          MAP_DEVICE_ID(LTD001),
          MAP_DEVICE_ID(LTD002),
          // Hue ambiance floor, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LFF001),
          // Hue ambiance table, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LTT001),
          // Hue ambiance downlight, Color Temperature Light, 2200K-6500K
          MAP_DEVICE_ID(LDT001),
          // Hue white wall washer, Dimmable Light, No Color
          MAP_DEVICE_ID(LDF002),
          // Hue white ceiling, Dimmable Light, No Color
          MAP_DEVICE_ID(LDF001),
          // Hue white floor, Dimmable Light, No Color
          MAP_DEVICE_ID(LDD002),
          // Hue white table, Dimmable Light, No Color
          MAP_DEVICE_ID(LDD001),
          // Hue white 1-10V, Dimmable Light, No Color
          MAP_DEVICE_ID(MWM001),
      }
  );

  return device_id_map;
}

uint32_t HueResponder::createVersionNumber() {
  // Remove all non-numeric characters from the version string
  const std::string allowed = "0123456789";
  std::string version_str;
  version_str.reserve(m_light->info.swVersion.size());
  for (const char &c : m_light->info.swVersion) {
    if (allowed.find_first_of(c) != std::string::npos) {
      version_str += c;
    }
  }
  unsigned long long version_ll = std::stoull(version_str);
  // Discard the extra bits
  uint32_t version = version_ll & 0xFFFFFFFF;

  return version;
}

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
