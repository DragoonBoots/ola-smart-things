//
// Created by dankeenan on 5/18/20.
//

#ifndef PLUGINS_HUE_SHOWFILE_H_
#define PLUGINS_HUE_SHOWFILE_H_

#include "olad/Preferences.h"
#include "lib/Bridge.h"

namespace ola {
namespace plugin {
namespace hue {

class ShowFile {
 public:
  explicit ShowFile(Preferences *preferences) :
      m_preferences(preferences) {}

  /**
   * Get the full patch.
   * @return
   */
  lib::LightList getPatch() { return m_lights; }

  /**
   * Get the patch for a specific light id.
   * @param lightId
   * @return
   */
  lib::Light getPatch(const lib::LightId &lightId) { return m_lights.at(lightId); }

  /**
   * Set the full patch, overwriting existing data.
   * @param lights
   */
  void setPatch(const lib::LightList &lights) { m_lights = lights; save(); }

  /**
   * Set the patch for a specific light.
   * @param light
   */
  void setPatch(const lib::Light &light) {
    if (m_lights.find(light.getLightId()) != m_lights.end()) {
      m_lights.erase(light.getLightId());
    }
    m_lights.insert({light.getLightId(), light});
    save();
  }

  /**
   * Load the showfile from disk.
   */
  bool load();

  /**
   * Save the showfile contents to disk.
   */
  bool save();

 private:
  static const char LIGHT_KEY_PREFIX[];

  Preferences *m_preferences;
  lib::LightList m_lights;

  // Pref key name generators
  static std::string key_prefix(const lib::LightId &light_id, const std::string &key) {
    return std::string(LIGHT_KEY_PREFIX) + "_" + std::to_string(light_id) + "_" + key;
  }
  static std::string key_light_id() { return std::string(LIGHT_KEY_PREFIX) + "_id"; }
  static std::string key_name(const lib::LightId &light_id) { return key_prefix(light_id, "name"); }
  static std::string key_address(const lib::LightId &light_id) { return key_prefix(light_id, "address"); }
  static std::string key_personality(const lib::LightId &light_id) { return key_prefix(light_id, "personality"); }
};

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif //PLUGINS_HUE_SHOWFILE_H_
