Hue Plugin
===============================

This plugin allows OLA to control Philips Hue lights.


## Config file: `ola-hue.conf`

`bridge_address = <ip>`
The Bridge's IP Address.

`device_name = <str>`
The device name to use when registering with the bridge.
