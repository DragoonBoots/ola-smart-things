//
// Created by dankeenan on 5/18/20.
//

#ifndef PLUGINS_HUE_HUERDM_H_
#define PLUGINS_HUE_HUERDM_H_

#include <memory>
#include <string>
#include <ola/rdm/NetworkManagerInterface.h>
#include <ola/rdm/RDMControllerInterface.h>
#include <ola/rdm/RDMEnums.h>
#include <ola/rdm/ResponderOps.h>
#include <ola/rdm/ResponderPersonality.h>
#include <ola/rdm/ResponderSensor.h>
#include <ola/rdm/UID.h>
#include "lib/Bridge.h"
#include "lib/Light.h"
#include "ShowFile.h"

namespace ola {
namespace plugin {
namespace hue {

class HueResponder : public rdm::RDMControllerInterface {
 public:
  explicit HueResponder(const rdm::UID &uid,
                        const std::shared_ptr<lib::Light> &light,
                        const std::shared_ptr<lib::Bridge> &bridge,
                        const std::shared_ptr<ShowFile> &showFile);

  void SendRDMRequest(rdm::RDMRequest *request, rdm::RDMCallback *callback);

 private:
  class RdmOps : public rdm::ResponderOps<HueResponder> {
   public:
    static RdmOps *get() {
      static auto *instance = new RdmOps;
      return instance;
    }
   private:
    RdmOps() : rdm::ResponderOps<HueResponder>(PARAM_HANDLERS) {}
  };

  class Personalities : public rdm::PersonalityCollection {
   public:
    static Personalities *get();
    const rdm::Personality *lookup(lib::PersonalityType personality) const;
    uint8_t lookupId(lib::PersonalityType personality) const;

   private:
    struct PersonalityInfo {
      unsigned int footprint;
      rdm::SlotDataCollection::SlotDataList slot_data;
      std::string name;
    };

    explicit Personalities(const PersonalityList &personalities) :
        rdm::PersonalityCollection(personalities), m_personalities(personalities) {}

    static PersonalityInfo persInfoNone();
    static PersonalityInfo persInfoRgbDirect();
    static PersonalityInfo persInfoRgbCalibrated();
    static PersonalityInfo persInfoCieXyz();

    const PersonalityList m_personalities;
  };

  const rdm::UID m_uid;
  std::shared_ptr<lib::Bridge> m_bridge;
  std::shared_ptr<lib::Light> m_light;
  uint16_t m_start_address;
  std::shared_ptr<ShowFile> m_show_file;
  bool m_identify_mode;
  Personalities* m_personalities;
  rdm::PersonalityManager m_personality_manager;

  /**
   * Map PIDs to handlers
   */
  static const rdm::ResponderOps<HueResponder>::ParamHandler PARAM_HANDLERS[];
  static const lib::PersonalityType DEFAULT_PERSONALITY = lib::PersonalityType::PersNone;

  /**
   * Map modelid (from the API) to RDM device ids.
   *
   * @return
   */
  static std::unordered_map<std::string, uint16_t> deviceIdMap();

  /**
   * Creates a version number from the light's version string
   *
   * This number is suitable for inclusion in RDM responses.
   * @return
   */
  uint32_t createVersionNumber();

  // Handlers
  rdm::RDMResponse *getDeviceInfo(const rdm::RDMRequest *request);
  rdm::RDMResponse *getSoftwareVersionLabel(const rdm::RDMRequest *request);
  rdm::RDMResponse *getAddress(const rdm::RDMRequest *request);
  rdm::RDMResponse *setAddress(const rdm::RDMRequest *request);
  rdm::RDMResponse *getPersonality(const rdm::RDMRequest *request);
  rdm::RDMResponse *setPersonality(const rdm::RDMRequest *request);
  rdm::RDMResponse *getPersonalityLabel(const rdm::RDMRequest *request);
  rdm::RDMResponse *getIdentify(const rdm::RDMRequest *request);
  rdm::RDMResponse *setIdentify(const rdm::RDMRequest *request);
  rdm::RDMResponse *getManufacturerLabel(const rdm::RDMRequest *request);
  rdm::RDMResponse *getDeviceLabel(const rdm::RDMRequest *request);
  rdm::RDMResponse *setDeviceLabel(const rdm::RDMRequest *request);
  rdm::RDMResponse *getDeviceModelDescription(const rdm::RDMRequest *request);
};

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */

#endif //PLUGINS_HUE_HUERDM_H_
