//
// Created by dankeenan on 5/18/20.
//

#include "ShowFile.h"

namespace ola {
namespace plugin {
namespace hue {

const char ShowFile::LIGHT_KEY_PREFIX[] = "show_light";

bool ShowFile::load() {
  m_lights.clear();
  if (!m_preferences->HasKey(key_light_id())) {
    // Blank showfile
    OLA_INFO << "Creating blank Hue patch.";
    return true;
  }
  for (const std::string &light_id_s : m_preferences->GetMultipleValue(key_light_id())) {
    const lib::LightId light_id = std::stoul(light_id_s);
    const std::string name = m_preferences->GetValue(key_name(light_id));
    const unsigned int address = std::stoul(m_preferences->GetValue(key_address(light_id)));
    const lib::PersonalityType
        personality = static_cast<lib::PersonalityType>(std::stoul(m_preferences->GetValue(key_personality(light_id))));
    m_lights.insert({light_id, lib::Light(light_id, name, address, personality)});
  }

  return true;
}

bool ShowFile::save() {
  // This creates several pref entries for each light
  // First clear the old data
  if (m_preferences->HasKey(key_light_id())) {
    for (const std::string &light_id_s : m_preferences->GetMultipleValue(key_light_id())) {
      const lib::LightId light_id = std::stoul(light_id_s);
      m_preferences->RemoveValue(key_name(light_id));
      m_preferences->RemoveValue(key_address(light_id));
      m_preferences->RemoveValue(key_personality(light_id));
    }
    m_preferences->RemoveValue(key_light_id());
  }

  // Add the new data
  for (const lib::LightList::value_type &item : m_lights) {
    const lib::LightId &light_id = item.first;
    const lib::Light &light = item.second;

    m_preferences->SetMultipleValue(key_light_id(), light_id);
    m_preferences->SetValue(key_name(light_id), light.getName());
    m_preferences->SetValue(key_address(light_id), light.getAddress());
    m_preferences->SetValue(key_personality(light_id), light.getPersonalityType());
  }
  return m_preferences->Save();
}

} /* namespace hue */
} /* namespace plugin */
} /* namespace ola */
