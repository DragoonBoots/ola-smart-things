#!/usr/bin/env sh

mkdir -p /home/buildroot/images
rm -R /home/buildroot/images/*
cp -Rfv -t /home/buildroot/images/ ${1}/*
