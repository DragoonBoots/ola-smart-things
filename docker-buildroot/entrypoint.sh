#!/usr/bin/env bash
set -xe

cd "${BUILDROOT_BASE}"
make O="${BUILDROOT_OUTPUT}" defconfig
cd "${BUILDROOT_OUTPUT}"
exec $@
