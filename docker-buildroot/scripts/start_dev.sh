#!/usr/bin/env bash
set -xe

docker build \
--target base \
-t olasmartthings/buildroot \
.

exec docker run -it \
-v "$(pwd)/external:/home/buildroot/buildroot/external" \
-v "$(pwd)/scripts:/home/buildroot/scripts/" \
-v "$(pwd)/output:/home/buildroot/output" \
-v "$(pwd)/..:/home/buildroot/src/olasmartthings:ro" \
olasmartthings/buildroot $@
